import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import ContractModel from '../Contract/ContractModel';
import { ProjectAssignmentAttributes } from './ProjectAssignmentAttributes';
import ProjectModel from './ProjectModel';

@Table({
	tableName: 'projectAssignment',
})
export default class ProjectAssignmentModel extends Model<ProjectAssignmentModel> implements ProjectAssignmentAttributes {

	@ForeignKey(() => ContractModel)
	@Column
	contractId: number;

	@ForeignKey(() => ProjectModel)
	@Column
	projectId: number;

	@Column
	position: string;

	@Column
	from: Date;

	@Column
	to: Date;

	@Column
	extent: number;
}
