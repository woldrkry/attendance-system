
export interface ProjectAssignmentToCreate {
	contractId: number;
	projectId: number;
	position: string;
	from: string;
	to: string;
	extent: number;
}

export interface ProjectAssignment extends ProjectAssignmentToCreate {
	id: number;
	createdAt: string;
	updatedAt: string;
}

export interface ProjectAssignmentAttributes {
	id?: number;
	contractId?: number;
	projectId?: number;
	position: string;
	from: Date;
	to: Date;
	extent: number;
	createdAt?: Date;
	updatedAt?: Date;
}
