import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { ProjectAssignmentAttributes } from './ProjectAssignmentAttributes';
import { ProjectAttributes } from './ProjectAttributes';
import ProjectAssignmentModel from './ProjectAssignmentModel';

@Table({
	tableName: 'project',
})
export default class ProjectModel extends Model<ProjectModel> implements ProjectAttributes {

	@HasMany(() => ProjectAssignmentModel)
	projectAssignments: ProjectAssignmentAttributes[];
	
	@Column
	name: string;
	
	@Column
	description: string;
	
	@Column
	from: Date;
	
	@Column
	to: Date;
	
	@Column
	hours: number;
}
