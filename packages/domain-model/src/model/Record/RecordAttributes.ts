import RecordType from './RecordType';

export interface RecordToCreate {
	employeeId: number;
	projectId: number;
	description?: string | null;
	type: RecordType;
	from: string;
	to: string;
}

export interface Record extends RecordToCreate {
	id: number;
	createdAt: Date;
	updatedAt: Date;
}

export type RecordToUpdate = Partial<Record> & Pick<Record, 'id'>;

export interface RecordAttributes {
	id?: number;
	projectId?: number;
	employeeId?: number;
	description?: string;
	type: RecordType;
	from: Date;
	to: Date;
	createdAt?: Date;
	updatedAt?: Date;
}
