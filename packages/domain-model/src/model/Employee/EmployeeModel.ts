import { Table, Column, Model, CreatedAt, UpdatedAt, HasOne, HasMany } from 'sequelize-typescript';
import EmployeeAttributes from './EmployeeAttributes';
import SimulationAttributes from '../Simulation/SimulationAttributes';
import SimulationModel from '../Simulation/SimulationModel';
import ContractModel from '../Contract/ContractModel';
import { ContractAttributes } from '../Contract/ContractAttributes';

@Table({
	tableName: 'employee',
})
export default class EmployeeModel extends Model<EmployeeModel> implements EmployeeAttributes {

	@Column
	name: string;
	
	@Column
	academicTitle: string;
	
	@Column
	dateOfBird: Date;

	@HasOne(() => SimulationModel)
	simulationData: SimulationAttributes;

	@HasMany(() => ContractModel)
	contracts: ContractAttributes[];

	@CreatedAt
	@Column
	createdAt!: Date;

	@UpdatedAt
	@Column
	updatedAt!: Date;
}
