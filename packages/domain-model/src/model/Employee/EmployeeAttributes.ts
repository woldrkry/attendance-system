import SimulationAttributes, { Simulation } from '../Simulation/SimulationAttributes';
import { ContractAttributes, Contract } from '../Contract/ContractAttributes';

export interface EmployeeToCreate {
	name: string;
	academicTitle: string | null;
	dateOfBird: Date;
}

export interface Employee extends EmployeeToCreate {
	id: number;
	simulationData: Simulation | null;
	contracts: Contract[];
	createdAt: Date;
	updatedAt: Date;
}

export default interface EmployeeAttributes {
	id?: number;
	name: string;
	academicTitle: string | null;
	dateOfBird: Date;
	simulationData?: SimulationAttributes | null;
	contracts?: ContractAttributes[] | null;
	createdAt?: Date;
	updatedAt?: Date;
}
