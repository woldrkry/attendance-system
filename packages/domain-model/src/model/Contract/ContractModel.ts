import { Column, Model, Table, ForeignKey, HasMany } from 'sequelize-typescript';
import EmployeeModel from '../Employee/EmployeeModel';
import { ContractAttributes } from './ContractAttributes';
import { ContractType } from './ContractType';
import { DataTypes } from 'sequelize';
import ProjectAssignmentModel from '../Project/ProjectAssignmentModel';
import { ProjectAssignmentAttributes } from '../Project/ProjectAssignmentAttributes';

@Table({
	tableName: 'contract',
})
export default class ContractModel extends Model<ContractModel> implements ContractAttributes {

	@Column({ type: DataTypes.STRING })
	type: ContractType;
	
	@Column
	extent: number;
	
	@Column
	from: Date;
	
	@Column
	to: Date;

	@ForeignKey(() => EmployeeModel)
	@Column
	employeeId: number;

	@HasMany(() => ProjectAssignmentModel)
	projectAssignments: ProjectAssignmentAttributes[];
}
