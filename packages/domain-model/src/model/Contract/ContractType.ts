
export type ContractType = 'Technician'
	| 'Administrative'
	| 'Pedagogue'
	| 'Scientist';
