import SimulationAttributes from '../model/Simulation/SimulationAttributes';
import EmployeeAttributes from '../model/Employee/EmployeeAttributes';
import EmployeeModel from '../model/Employee/EmployeeModel';
import SimulationModel from '../model/Simulation/SimulationModel';
import { ContractAttributes } from '../model/Contract/ContractAttributes';
import ContractModel from '../model/Contract/ContractModel';
import { ProjectAttributes } from '../model/Project/ProjectAttributes';
import ProjectModel from '../model/Project/ProjectModel';
import { ProjectAssignmentAttributes } from '../model/Project/ProjectAssignmentAttributes';
import ProjectAssignmentModel from '../model/Project/ProjectAssignmentModel';
import { RecordAttributes } from '../model/Record/RecordAttributes';
import RecordModel from '../model/Record/RecordModel';

enum EMPLOYEE_DEMO_ID {
	PERSON1,
	PERSON2,
	PERSON3,
}

enum CONTRACT_DEMO_ID {
	CONTRACT1,
	CONTRACT2,
	CONTRACT3,
	CONTRACT4,
	CONTRACT5,
	CONTRACT6,
}

enum PROJECT_DEMO_ID {
	PROJECT1,
	PROJECT2,
	PROJECT3,
}

interface ContractDemoAttributes extends ContractAttributes {
	demoId: CONTRACT_DEMO_ID;
	employeeDemoId: EMPLOYEE_DEMO_ID;
}

interface SimulationDemoAttributes extends SimulationAttributes {
	employeeDemoId: EMPLOYEE_DEMO_ID;
}

interface EmployeeDemoAttributes extends EmployeeAttributes {
	demoId: EMPLOYEE_DEMO_ID;
}

interface ProjectDemoAttributes extends ProjectAttributes {
	demoId: PROJECT_DEMO_ID;
}

interface ProjectAssignmentDemoAttributes extends ProjectAssignmentAttributes {
	contractDemoId: CONTRACT_DEMO_ID;
	projectDemoId: PROJECT_DEMO_ID;
}

interface RecordDemoAttributes extends RecordAttributes {
	projectDemoId: PROJECT_DEMO_ID;
	employeeDemoId: EMPLOYEE_DEMO_ID; 
}

const simulations: SimulationDemoAttributes[] = [
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		workingHoursBeginningMinutesFrom: 530, //8:50
		workingHoursBeginningMinutesTo: 555, //9:15
		workingHoursLengthMinutesFrom: 465, //7:45
		workingHoursLengthMinutesTo: 510, //8:30
		weekDaysJSONArray: JSON.stringify([1, 2, 3, 4]), // Mon, Tue, Wed, Thu
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON2,
		workingHoursBeginningMinutesFrom: 550, //9:10
		workingHoursBeginningMinutesTo: 570, //9:30
		workingHoursLengthMinutesFrom: 460, //7:40
		workingHoursLengthMinutesTo: 485, //8:05
		weekDaysJSONArray: JSON.stringify([1, 3, 6]), // Mon, Wed, Sat
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		workingHoursBeginningMinutesFrom: 550, //9:10
		workingHoursBeginningMinutesTo: 570, //9:30
		workingHoursLengthMinutesFrom: 460, //7:40
		workingHoursLengthMinutesTo: 485, //8:05
		weekDaysJSONArray: JSON.stringify([1, 3, 6]), // Mon, Wed, Sat
	},
];

const employees: EmployeeDemoAttributes[] = [
	{ 
		demoId: EMPLOYEE_DEMO_ID.PERSON1,
		name: 'John Doe',
		academicTitle: 'MS',
		dateOfBird: new Date("1990-10-10T10:10"),
	},
	{ 
		demoId: EMPLOYEE_DEMO_ID.PERSON2,
		name: 'Tom Smith',
		academicTitle: 'MS',
		dateOfBird: new Date("1991-11-11T11:11"),
	},
	{
		demoId: EMPLOYEE_DEMO_ID.PERSON3,
		name: 'Jane Black',
		academicTitle: 'MA',
		dateOfBird: new Date("1992-12-12T12:12"),
	},
];

const contracts: ContractDemoAttributes[] = [
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT1,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		extent: 0.5,
		from: new Date("2020-1-1"),
		to: new Date("2023-1-1"),
		type: 'Administrative',
	},
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT2,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		extent: 0.5,
		from: new Date("2020-1-1"),
		to: new Date("2023-1-1"),
		type: 'Technician',
	},
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT3,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON2,
		extent: 1,
		from: new Date("2017-1-1"),
		to: new Date("2019-1-1"),
		type: 'Administrative',
	},
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT4,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		extent: 0.7,
		from: new Date("2020-1-1"),
		to: new Date("2022-1-1"),
		type: 'Pedagogue',
	},
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT5,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		extent: 0.3,
		from: new Date("2020-1-1"),
		to: new Date("2022-1-1"),
		type: 'Scientist',
	},
	{
		demoId: CONTRACT_DEMO_ID.CONTRACT6,
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		extent: 1,
		from: new Date("2022-1-2"),
		to: new Date("2023-1-1"),
		type: 'Scientist',
	},
];

const projects: ProjectDemoAttributes[] = [
	{
		demoId: PROJECT_DEMO_ID.PROJECT1,
		name: 'Project 1',
		description:'Description of the awesome Project 2',
		from: new Date("2015-1-1"),
		to: new Date("2025-1-1"),
		hours: 900,
	},
	{
		demoId: PROJECT_DEMO_ID.PROJECT2,
		name: 'Project 2',
		description:'Description of the awesome Project 2',
		from: new Date("2019-1-1"),
		to: new Date("2023-1-1"),
		hours: 840,
	},
	{
		demoId: PROJECT_DEMO_ID.PROJECT3,
		name: 'Project 3',
		description:'Description of the awesome Project 3',
		from: new Date("2020-1-1"),
		to: new Date("2030-1-1"),
		hours: 700,
	},
];

const projectAssignments: ProjectAssignmentDemoAttributes[] = [
	{
		contractDemoId: CONTRACT_DEMO_ID.CONTRACT1,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		position: 'researcher',
		extent: 0.5,
		from: new Date("2020-1-1"),
		to: new Date("2030-1-1"),
	},
	{
		contractDemoId: CONTRACT_DEMO_ID.CONTRACT2,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		position: 'administrator',
		extent: 0.5,
		from: new Date("2020-1-1"),
		to: new Date("2030-1-1"),
	},
	{
		contractDemoId: CONTRACT_DEMO_ID.CONTRACT4,
		projectDemoId: PROJECT_DEMO_ID.PROJECT2,
		position: 'scientist',
		extent: 1,
		from: new Date("2020-1-1"),
		to: new Date("2030-1-1"),
	},
];

const records: RecordDemoAttributes[] = [
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		from: new Date("2020-01-10T10:10"),
		to: new Date("2020-01-10T12:10"),
		type: 'office',
		description: 'Fixing bugs',
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		from: new Date("2020-01-11T10:10"),
		to: new Date("2020-01-11T12:10"),
		type: 'office',
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON1,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		from: new Date("2020-02-10T10:10"),
		to: new Date("2020-02-10T12:10"),
		type: 'office',
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		from: new Date("2020-01-10T10:10"),
		to: new Date("2020-01-10T12:10"),
		type: 'office',
		description: 'Add new feature',
	},
	{
		employeeDemoId: EMPLOYEE_DEMO_ID.PERSON3,
		projectDemoId: PROJECT_DEMO_ID.PROJECT1,
		from: new Date("2020-01-10T10:10"),
		to: new Date("2020-01-10T12:10"),
		type: 'vacation',
	},
];

async function initializeDataDemo() {
	const createdEmployees = await initializeEmployeesDemo(employees);
	await initializeSimulationsDemo(simulations, createdEmployees);
	const createdContracts = await initializeContractDemo(contracts, createdEmployees);
	const createdProjects = await initializeProjectsDemo(projects);
	await initializeProjectsAssignmentsDemo(projectAssignments, createdProjects, createdContracts);
	await initializeRecordsDemo(records, createdEmployees, createdProjects);
}

async function initializeEmployeesDemo(employees: EmployeeDemoAttributes[]) {
	const savedEmployees: EmployeeDemoAttributes[] = [];

	for (const employee of employees) {
		const savedEmployeeModel = await EmployeeModel.create(employee);
		const savedEmployee = {
			...employee,
			id: savedEmployeeModel.id,
		}
		savedEmployees.push(savedEmployee);
	}

	return savedEmployees;
}

async function initializeSimulationsDemo(simulations: SimulationDemoAttributes[], employees: EmployeeDemoAttributes[]) {
	const savedSimulations = [];

	for (const simulation of simulations) {
		const employee = employees.find((employee) => employee.demoId === simulation.employeeDemoId);
		const savedSimulationModel = await SimulationModel.create({
			...simulation,
			employeeId: employee.id,
		});
		const savedSimulation = {
			...simulation,
			id: savedSimulationModel.id,
		}
		savedSimulations.push(savedSimulation);
	}

	return savedSimulations;
}

async function initializeContractDemo(contracts: ContractDemoAttributes[], employees: EmployeeDemoAttributes[]) {
	const savedContracts: ContractDemoAttributes[] = [];

	for (const contract of contracts) {
		const employee = employees.find((employee) => employee.demoId === contract.employeeDemoId);
		const savedContractModel = await ContractModel.create({
			...contract,
			employeeId: employee.id,
		} as ContractDemoAttributes);
		const savedContract: ContractDemoAttributes = {
			...contract,
			id: savedContractModel.id,
		};
		savedContracts.push(savedContract);
	}

	return savedContracts;
}

async function initializeProjectsDemo(projects: ProjectDemoAttributes[]) {
	const savedProjects: ProjectDemoAttributes[] = [];

	for (const project of projects) {
		const savedProjectModel = await ProjectModel.create(project);
		savedProjectModel
		savedProjects.push({
			...project,
			id: savedProjectModel.id,
		});
	}

	return savedProjects;
}

async function initializeProjectsAssignmentsDemo(
	projectsAssignments: ProjectAssignmentDemoAttributes[],
	projects: ProjectDemoAttributes[],
	contracts: ContractDemoAttributes[],
) {
	const savedProjectAssignments: ProjectAssignmentDemoAttributes[] = [];

	for (const projectsAssignment of projectsAssignments) {
		const project = projects.find((project) => project.demoId === projectsAssignment.projectDemoId);
		const contract = contracts.find((contract) => contract.demoId === projectsAssignment.contractDemoId);
		const savedProjectAssignmentModel = await ProjectAssignmentModel.create({
			...projectsAssignment,
			projectId: project.id,
			contractId: contract.id,
		} as ProjectAssignmentAttributes);
		const savedProjectAssignment: ProjectAssignmentDemoAttributes = {
			...projectsAssignment,
			id: savedProjectAssignmentModel.id,
		};
		savedProjectAssignments.push(savedProjectAssignment);
	}

	return savedProjectAssignments;
}

async function initializeRecordsDemo(
	records: RecordDemoAttributes[],
	employees: EmployeeDemoAttributes[],
	projects: ProjectDemoAttributes[],
) {
	const savedRecords: RecordDemoAttributes[] = [];

	for (const record of records) {
		const employee = employees.find((employee) => employee.demoId === record.employeeDemoId);
		const project = projects.find((project) => project.demoId === record.projectDemoId);
		const savedRecordModel = await RecordModel.create({
			...record,
			employeeId: employee.id,
			projectId: project.id,
		} as RecordDemoAttributes);
		const savedRecord: RecordDemoAttributes = {
			...record,
			id: savedRecordModel.id,
		};
		savedRecords.push(savedRecord);
	}

	return savedRecords;
}

export default initializeDataDemo;
