const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const url = require('url');
const fs = require('fs-extra');
const { channels } = require('../src/shared/constants');
const { dialog } = require('electron');

let mainWindow;

function createWindow () {
  console.log(process.env.NODE_ENV);
  const startUrl = process.env.ELECTRON_START_URL || url.format({
    pathname: process.env.NODE_ENV === 'dev'
      ? path.join(__dirname, '../build/index.html')
      : path.join(__dirname, '../index.html'),
    protocol: 'file:',
    slashes: true,
  });
  console.log(startUrl);
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      devTools: process.env.NODE_ENV === 'dev',
    },
  });
  mainWindow.webContents.openDevTools({
    mode: 'detach',
  });
  mainWindow.loadURL(startUrl);
  mainWindow.on('closed', function () {
    mainWindow = null;
  });
}
app.on('ready', createWindow);
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on(channels.APP_INFO, (event) => {
  event.sender.send(channels.APP_INFO, {
    appName: app.getName(),
    appVersion: app.getVersion(),
  });
});

ipcMain.on('Dialog.Save.File.Request', async (event, options) => {
  const openDialogResult = await dialog.showSaveDialog({
    defaultPath: options.defaultFileName,
  });

  console.log(openDialogResult);

  event.sender.send(
    'Dialog.Save.File.Succeed',
    openDialogResult,
  );
});

ipcMain.on('FileSystem.File.Write.Request', async (event, options) => {
  const { filePath, data } = options;

  if (filePath && data) {
    try {
      await fs.writeFile(filePath, Buffer.from(data), { encoding: 'binary' });

      event.sender.send(
        'FileSystem.File.Write.Succeed',
      );
    } catch (error) {
      event.sender.send(
        'FileSystem.File.Write.Failed',
      );
    }
  }
});
