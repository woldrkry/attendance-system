import { Record, RecordToUpdate } from 'domain-model/dist/model/Record/RecordAttributes';
import { doPut } from '../doRequest';

export const RESOURCE = `records`;

export default function updateRecord(baseUrl: string, data: RecordToUpdate) {
	const url = `${baseUrl}/${RESOURCE}`;
	return doPut<Record, RecordToUpdate>(url, data);
}
