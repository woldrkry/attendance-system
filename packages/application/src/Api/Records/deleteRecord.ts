import { Record, RecordToUpdate } from 'domain-model/dist/model/Record/RecordAttributes';
import { doDelete } from '../doRequest';

export const RESOURCE = `records`;

export default function deleteRecord(baseUrl: string, id: number) {
	const url = `${baseUrl}/${RESOURCE}`;
	return doDelete<Record, RecordToUpdate>(url, {id});
}
