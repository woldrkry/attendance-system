import ApiActions, {
	ApiBaseUrlChange,
} from './apiActions';
import { baseUrlDefault } from './api';

export interface IApiState {
	baseUrl: string;
}

const initState: IApiState = {
	baseUrl: baseUrlDefault,
}

export default function (state: IApiState = initState, action: ApiActions): IApiState {

	switch (action.type) {
		case ApiBaseUrlChange:
			return {
				...state,
				baseUrl: action.baseUrl,
			};
	
		default:
			return state;
	}
}