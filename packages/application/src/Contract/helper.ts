import { ContractAttributes } from 'domain-model/dist/model/Contract/ContractAttributes';

export default function ContractLocalIdGenerator() {
	let id = 0;
	return {
		generate: () => {
			return id++;
		},

		reset: () => {
			id = 0;
		},
	};
}

export type ContractsMap = {[id: number]: ContractAttributes | undefined};

export function getContractsMap(employees: ContractAttributes[]) {
	const map = employees.reduce<ContractsMap>((prev, curr) => ({ ...prev, [curr.id!]: curr }), {});

	return map;
}
