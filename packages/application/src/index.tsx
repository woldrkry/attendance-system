import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import 'normalize.css/normalize.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/table/lib/css/table.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './index.css';
import { history } from './store';
import * as serviceWorker from './serviceWorker';
import { configureAppStore } from './store';
import { routes } from './Router/routes';

const store = configureAppStore();
const toRender = (
	<Provider store={store}>
		<ConnectedRouter history={history}>
			{routes}
		</ConnectedRouter>
	</Provider>
);

ReactDOM.render(toRender, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
