import { ProjectAttributes, Project, ProjectToCreate } from 'domain-model/dist/model/Project/ProjectAttributes';

export const ProjectsLoadRequest = 'Projects.Load.Request';
export interface ProjectsLoadRequest {
	type: typeof ProjectsLoadRequest;
}

export const ProjectsLoadSuccess = 'Projects.Load.Success';
export interface ProjectsLoadSuccess {
	type: typeof ProjectsLoadSuccess;
	response: Project[];
}

export const ProjectsLoadError = 'Projects.Load.Error';
export interface ProjectsLoadError {
	type: typeof ProjectsLoadError;
	message: string;
}

export const NewProjectCreate = 'New.Project.Create';
export interface NewProjectCreate {
	type: typeof NewProjectCreate;
	data: ProjectToCreate;
}

type ProjectsActions = ProjectsLoadRequest
	| ProjectsLoadSuccess
	| ProjectsLoadError;

export default ProjectsActions;
