import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { IState } from '../reducer';
import ProjectsTable, { I_ROW as ProjectsTableRow } from '../components/ProjectsTable/ProjectsTable';
import { Project } from 'domain-model/dist/model/Project/ProjectAttributes';
import { ThunkDispatch } from '../store';
import { ProjectsLoad } from './projectsThunk';
import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { EmployeesLoad } from '../Employees/employeesThunk';
import { Button, Link } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { NEW_PROJECT_PATH } from '../Router/routes';

const NEW_BUTTON_LABEL = 'Nový projekt';

interface IOwnProps {}
interface IStateProps {
	employees: EmployeeAttributes[]; //TODO: change to pure Employee
	projects: Project[];
}

interface IDispatchProps {

}

export type IProps = IOwnProps & IStateProps & IDispatchProps;

function Projects(props: IProps) {
	const {
		employees,
		projects,
	} = props;


	const projectsTableData = projects.map((project): ProjectsTableRow => {
		const uniqContractIds = _
			.uniqBy(project.projectAssignments, (a) => a.contractId)
			.map((assignment) => assignment.contractId);
		const employeesByContractWithUndefined = employees.map((employee) => {
			const contract = employee.contracts?.find((contract) => {
				return uniqContractIds.includes(contract.id!);
			});

			if (contract) {
				return employee;
			} else {
				return undefined;
			}
		});

		const employeesByContract = _.without(employeesByContractWithUndefined, undefined);
		const employeesCount = employeesByContract.length;

		return {
			id: project.id,
			name: project.name,
			employeesCount: employeesCount,
			from: project.from,
			to: project.to,
		};
	});

	return (
		<div>
			<Button
				variant='contained'
				color='primary'
				size='small'
				component={NavLink}
				to={NEW_PROJECT_PATH}
			>
				{NEW_BUTTON_LABEL}
			</Button>
			<br/>
			<br/>
			<ProjectsTable
				data={projectsTableData}
			/>
		</div>
	);
}

export default connect(
	(state: IState): IStateProps => ({
		employees: state.employees.data,
		projects: Object.values(state.projects.data),
	}),
	(dispatch: ThunkDispatch): IDispatchProps => {
		dispatch(EmployeesLoad());
		dispatch(ProjectsLoad());
		return {
			//TODO:
		};
	},
)(Projects);
