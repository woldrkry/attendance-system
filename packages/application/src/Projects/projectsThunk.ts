import { ThunkAction } from '../store';
import { ProjectsLoadSuccess, ProjectsLoadRequest, ProjectsLoadError } from './projectsActions';
import { Project, ProjectToCreate } from 'domain-model/dist/model/Project/ProjectAttributes';
import { doGet } from '../Api/doRequest';
import createProject from '../Api/Projects/createProject';

export const ProjectsLoad = (): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const { api } = state;
	const apiBaseUrl = api.baseUrl;
	const resource = 'projects';
	const url = `${apiBaseUrl}/${resource}`;

	try {
		dispatch({
			type: ProjectsLoadRequest,
		} as ProjectsLoadRequest);
		const projects = await doGet<Project[]>(url);
		dispatch<ProjectsLoadSuccess>({
			type: ProjectsLoadSuccess,
			response: projects,
		});
	} catch (error) {
		dispatch({
			type: ProjectsLoadError,
			message: error.message,
		} as ProjectsLoadError)
	}
};

export const NewProjectCreate = (data: ProjectToCreate): ThunkAction => {
	return async (_, getState) => {
		const state = getState();
		const baseUrl = state.api.baseUrl;

		try {
			await createProject(
				baseUrl,
				data,
			);
		} catch (error) {
			//dispatch error;
		}
	};
}
