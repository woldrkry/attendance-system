import React from 'react';
import { Button } from '@material-ui/core';
import ProjectDetailEmployeesTable from '../components/ProjectDetailEmployeesTable/ProjectDetailEmployeesTable';
import { useParams } from 'react-router-dom';

const ADD_EMPLOYEE_BUTTON_LABEL = 'Přidat zaměstnance';
const GENERATE_PROJECT_WORKSHEET_BUTTON_LABEL = 'Generovat přehled projektu';

export default function ProjectDetail() {
	const { id } = useParams();

	const parsedId = id ? parseInt(id) : NaN;

	if (parsedId === NaN) {
		return (
			<div></div>
		);
	} else {
		return (
			<div>
				<Button
					variant='contained'
					color='primary'
					size='small'
					disabled
				>
					{ADD_EMPLOYEE_BUTTON_LABEL}
				</Button>
				<Button
					variant='contained'
					color='primary'
					size='small'
					disabled
				>
					{GENERATE_PROJECT_WORKSHEET_BUTTON_LABEL}
				</Button>
				<ProjectDetailEmployeesTable id={parsedId}/>
			</div>
		);
	}
}
