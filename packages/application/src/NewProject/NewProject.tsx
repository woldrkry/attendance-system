import React from 'react';
import NewProjectForm from '../components/NewProjectForm/NewProjectForm';

export default function NewProject() {
	return (
		<div>
			<NewProjectForm />
		</div>
	);
}
