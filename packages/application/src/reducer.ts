import { History } from 'history';
import { combineReducers } from 'redux'
import { connectRouter, RouterState } from 'connected-react-router';
import employeesReducer, { IEmployeesState } from './Employees/employeesReducer';
import apiReducer, { IApiState } from './Api/apiReducer';
import projectsReducer, { IProjectsState } from './Projects/projectsReducer';
import newEmployeeReducer, { INewEmployeeState } from './NewEmployee/newEmployeeReducer';
import recordsReducer, { IRecordsState } from './Records/recordsReducer';

export interface IState {
	router: RouterState,
	employees: IEmployeesState,
	newEmployee: INewEmployeeState,
	projects: IProjectsState,
	api: IApiState,
	records: IRecordsState,
} 

export const createRouterReducer = (history: History) => combineReducers<IState>({
	router: connectRouter(history),
	employees: employeesReducer,
	projects: projectsReducer,
	api: apiReducer,
	newEmployee: newEmployeeReducer,
	records: recordsReducer,
});
