import React from 'react';
import PrivateLayout from '../components/PrivateLayout/PrivateLayout';
import { Route, RouteProps } from 'react-router-dom';

export interface IProps {
	children: React.ReactNode
}

export default function PrivateRoute(props: IProps & RouteProps) {
	const { children } = props;

	const wrapped = (
		<PrivateLayout>
			{children}
		</PrivateLayout>
	);

	return (
		<Route {...props} children={undefined} render={() => wrapped}/>
	);
}
