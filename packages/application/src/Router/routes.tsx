import React from 'react';
import { Switch } from "react-router-dom";
import Login from '../Login/Login';
import Employees from '../Employees/Employees';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import NewEmployee from '../NewEmployee/NewEmployee';
import Projects from '../Projects/Projects';
import NewProject from '../NewProject/NewProject';
import ProjectDetail from '../Projects/ProjectDetail';
import EmployeeDetail from '../Employees/EmployeeDetail';

export const ROOT_PATH = '/';
export const EMPLOYEES_PATH = '/employees';
export const NEW_EMPLOYEE_PATH = '/employee/new';
export const EMPLOYEE_DETAIL_PATH = '/employee/:id';
export const PROJECTS_PATH = '/projects';
export const NEW_PROJECT_PATH = '/project/new';
export const PROJECT_DETAIL_PATH = '/project/:id';

export const routes = (
	<Switch>
		<PublicRoute path={ROOT_PATH} exact>
			<Login/>
		</PublicRoute>
		<PrivateRoute path={EMPLOYEES_PATH}>
			<Employees/>
		</PrivateRoute>
		<PrivateRoute path={NEW_EMPLOYEE_PATH}><NewEmployee/></PrivateRoute>
		<PrivateRoute path={EMPLOYEE_DETAIL_PATH}><EmployeeDetail/></PrivateRoute>
		<PrivateRoute path={PROJECTS_PATH}>
			<Projects/>
		</PrivateRoute>
		<PrivateRoute path={NEW_PROJECT_PATH}>
			<NewProject/>
		</PrivateRoute>
		<PrivateRoute path={PROJECT_DETAIL_PATH}>
			<ProjectDetail/>
		</PrivateRoute>
	</Switch>
);
