import React from 'react';
import { ColumnHeaderCell, IColumnHeaderRenderer } from '@blueprintjs/table';

export function getHeader(label: string): IColumnHeaderRenderer {
	return () => (
		<ColumnHeaderCell name={label}/>
	);
}
