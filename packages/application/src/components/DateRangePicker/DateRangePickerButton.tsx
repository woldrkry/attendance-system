import React from 'react';
import { IProps as IDatePickerProps, DateRangePicker } from './DateRangePicker';
import { Button, Popover, IButtonProps } from '@blueprintjs/core';

import styles from './DateRangePickerButton.module.css';

export interface IProps extends IDatePickerProps, IButtonProps {
	children?: React.ReactNode;
}

export function DateRangePickerButton(props: IProps) {
	const { onChange } = props;

	const onPopoverContentClick = (event: React.MouseEvent) => {
		event.stopPropagation();
	}

	return (
		<Popover>
			<Button minimal {...props} onChange={() => undefined}></Button>
			<div
				className={styles.datePickerContainer}
				onClick={onPopoverContentClick}
			>
				<DateRangePicker
					onChange={onChange}
				/>
			</div>
		</Popover>
	);
}
