import React from 'react';
import { connect } from 'react-redux';
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';
import MaterialTable, { Column } from 'material-table';
import { Record, RecordToCreate } from 'domain-model/dist/model/Record/RecordAttributes';
import RecordType from 'domain-model/dist/model/Record/RecordType';
import { RecordsLoad, NewRecordCreate, RecordUpdate, RecordDelete } from '../../Records/recordsThunk';

const TITLE_LABEL = 'Docházka';
const PROJECT_LABEL = 'Projekt';
const DESCRIPTION_LABEL = 'Popis záznamu';
const RECORD_TYPE_LABEL = 'Druh';
const DATE_FROM_LABEL = 'Od';
const DATE_TO_LABEL = 'Do';

export interface I_ROW {
	id: number;
	project: string,
	description: string | null | undefined,
	type: string,
	from: string | Date,
	to: string | Date,
}

const getColumns = (): Column<I_ROW>[] => {
	return [
		{ title: PROJECT_LABEL, field: 'project', editable: 'never', emptyValue: '' },
		{ title: DESCRIPTION_LABEL, field: 'description' },
		{ title: RECORD_TYPE_LABEL, field: 'type' },
		{ title: DATE_FROM_LABEL, field: 'from', type: 'datetime'},
		{ title: DATE_TO_LABEL, field: 'to', type: 'datetime'},
	];
};

export interface IOwnProps {
	employeeId: number;
	projectId: number;
	showFrom: Date | null;
	showTo: Date | null;
}

export interface IStateProps {
	data: I_ROW[];
}

export interface IDispatchProps {
	create: (data: RecordToCreate) => void;
	update: (id: number, data: RecordToCreate) => void;
	remove: (id: number) => void;
}

export type IProps = IStateProps & IDispatchProps & IOwnProps;

export function EmployeeDetailContainer(props: IProps) {
	const { projectId, employeeId, data, create, update, remove } = props;
	const columns = getColumns();

	const onRowAdd = async (newData: I_ROW) => {
		console.log('onRowAdd', 'newData', newData);
		const record = convert(newData, projectId, employeeId);
		create(record);
	};

	const onRowUpdate = async (newData: I_ROW, oldData?: I_ROW) => {
		console.log('onRowUpdate', 'newData', newData);
		console.log('oldData', oldData);
		const contract = convert(newData, projectId, employeeId);
		update(newData.id, contract);
	};

	const onRowDelete = async (oldData: I_ROW) => {
		console.log('onRowDelete', 'newData', oldData);
		remove(oldData.id);
	};

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			editable={{
				onRowAdd,
				onRowUpdate,
				onRowDelete,
			}}
		/>
	);
}

export default connect(
	(state: IState, props: IOwnProps): IStateProps => {
		const { employeeId } = props;
		const projects = state.projects.data;
		const records = Object.values(state.records.data);
		const employeeRecords: Record[] = [];

		records.forEach((record) => {
			if (record.employeeId === employeeId) {
				employeeRecords.push(record);
			}
		});

		const tableData = employeeRecords.map<I_ROW>((record) => ({
			id: record.id,
			project: projects[record.projectId].name,
			description: record.description,
			type: record.type,
			from: record.from,
			to: record.to,
		}));

		return {
			data: tableData,
		};
	},
	(dispatch: ThunkDispatch): IDispatchProps => {
		dispatch(RecordsLoad());
		return {
			create: (data: RecordToCreate) => {
				dispatch(NewRecordCreate(data));
			},
			update: (id: number , data: RecordToCreate) => {
				dispatch(RecordUpdate({ id, ...data }));
			},
			remove: (id: number) => {
				dispatch(RecordDelete(id));
			},
		};
	}
)(EmployeeDetailContainer);

export function convert(data: I_ROW, projectId: number, employeeId: number): RecordToCreate {
	return {
		employeeId,
		projectId,
		description: data.description,
		type: data.type as RecordType,
		from: typeof data.from === 'string' ? data.from : data.from?.toISOString(),
		to: typeof data.to === 'string' ? data.to : data.to?.toISOString(),
	};
}
