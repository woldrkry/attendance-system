import React from 'react';
import { Card, Elevation, Button } from '@blueprintjs/core';

export interface IProps {
	name: string;
	children: React.ReactNode;
}

export default function FormGroup(props: IProps) {
	const { name, children } = props;

	return (
		<div>
			<span>{name}</span>
			{children}
		</div>
	);
}
