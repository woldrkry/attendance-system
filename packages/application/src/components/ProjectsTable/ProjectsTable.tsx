import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { useHistory } from 'react-router-dom';
import { PROJECTS_PATH } from '../../Router/routes';

const TITLE_LABEL = 'Projekty';
const NAME_LABEL = 'Název';
const EMPLOYEES_COUNT_LABEL = 'Počet pracovníků';
const FROM_DATE_LABEL = 'Datum zahájení';
const TO_DATE_LABEL = 'Datum ukončení';

export interface I_ROW {
	id: number;
	name: string;
	employeesCount: number;
	from: Date | string;
	to: Date | string;
}

const getColumns = (): Column<I_ROW>[] => {
	return [
		{ title: NAME_LABEL, field: 'name' },
		{ title: EMPLOYEES_COUNT_LABEL, field: 'employeesCount' },
		{ title: FROM_DATE_LABEL, field: 'from', type: 'date' },
		{ title: TO_DATE_LABEL, field: 'to', type: 'date' },
	];
};

export interface IProps {
	data: I_ROW[];
}

export default function ProjectsTable(props: IProps) {
	const history = useHistory();
	const { data } = props;
	const columns = getColumns();

	const onRowClick = (_?: React.MouseEvent, data?: I_ROW) => {
		history.push(`/project/${data?.id}`);
	}

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			onRowClick={onRowClick}
		/>
	);
}
