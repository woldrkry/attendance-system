import React from 'react';
import MainMenu from '../MainMenu/MainMenu';

import styles from './PrivateLayout.module.css';

export interface IProps {
	children: React.ReactNode
}

export default function PrivateLayout(props: IProps) {
	const { children } = props;

	return (
		<div className={styles.container}>
			<div className={styles.applicationContainer}>
				<div>
					<MainMenu />
				</div>
				<div className={styles.contentContainer}>
					{children}
				</div>
			</div>
		</div>
	);
}
