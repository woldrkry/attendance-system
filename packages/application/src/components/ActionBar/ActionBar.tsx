import React from 'react';

import styles from './ActionBar.module.css';

export interface IProps {
	children: React.ReactNode;
}

export default function ActionBar(props: IProps) {
	const { children } = props;

	return (
		<div className={styles.container}>
			{children}
		</div>
	);
}
