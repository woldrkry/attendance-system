import React from 'react';
import MaterialTable, { Column } from 'material-table';
import { ContractToCreate, Contract } from 'domain-model/dist/model/Contract/ContractAttributes';
import { Project } from 'domain-model/dist/model/Project/ProjectAttributes';
import { ProjectAssignment } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

const TITLE_LABEL = 'Přiřazení projektů';
const CONTRACT_LABEL = 'Úvazek';
const NAME_LABEL = 'Název';
const EXTENT_LABEL = 'Úvazek';
const FROM_DATE_LABEL = 'Od';
const TO_DATE_LABEL = 'Do';

export interface I_ROW {
	id: number | undefined;
	contractId: string | undefined;
	projectId: string | undefined;
	extent: number | undefined; //TODO: string
	from: Date | string | undefined;
	to: Date | string | undefined;
}

const getColumns = (
	contracts: (Partial<Contract> & { id: number })[],
	projects: Project[],
): Column<I_ROW>[] => {
	const contractsIndicesLookup: {[id: number]: number} = {};
	contracts.forEach((contract) => contractsIndicesLookup[contract.id] = contract.id);
	const projectsNamesLookup: {[id: number]: string} = {};
	projects.forEach((project) => projectsNamesLookup[project.id] = project.name);

	return [
		{ title: CONTRACT_LABEL, field: 'contractId', lookup: contractsIndicesLookup },
		{ title: NAME_LABEL, field: 'projectId', lookup: projectsNamesLookup },
		{ title: EXTENT_LABEL, field: 'extent', type: 'numeric' },
		{ title: FROM_DATE_LABEL, field: 'from', type: 'date' },
		{ title: TO_DATE_LABEL, field: 'to', type: 'date' },
	];
};

export interface IProps {
	contracts: (Partial<Contract> & { id: number })[],
	projects: Project[],
	data: I_ROW[];
	create: (data: Partial<ProjectAssignment>) => void;
	update: (index: number, data: Partial<ProjectAssignment>) => void;
	remove: (index: number) => void;
}

export default function ProjectsAssignmentsTable(props: IProps) {
	const { contracts, projects, data, create, update, remove } = props;
	const columns = getColumns(contracts, projects);

	const onRowAdd = async (newData: I_ROW) => {
		const assignment = convert(newData, projects);
		create(assignment);
	};

	const onRowUpdate = async (newData: I_ROW, oldData?: I_ROW) => {
		const index = oldData ? data.indexOf(oldData) : -1;
		const assignment = convert(newData, projects);
		update(index, assignment);
	};

	const onRowDelete = async (oldData: I_ROW) => {
		const index = data.indexOf(oldData);
		remove(index);
	};

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
			editable={{
				onRowAdd,
				onRowUpdate,
				onRowDelete,
			}}
		/>	
	);
}

export function convert(data: I_ROW, projects: Project[]): Partial<ProjectAssignment> {
	const projectId = data.projectId ? parseInt(data.projectId) : undefined;
	return {
		...data,
		contractId: data.contractId ? parseInt(data.contractId) : undefined,
		projectId,
		from: typeof data.from === 'string' ? data.from : data.from?.toISOString(),
		to: typeof data.to === 'string' ? data.to : data.to?.toISOString(),
	};
}
