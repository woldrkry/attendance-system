import React from 'react';
import { Button, Icon, IconName } from '@blueprintjs/core';
import { NavLink } from 'react-router-dom';
import { ROOT_PATH, EMPLOYEES_PATH, PROJECTS_PATH } from '../../Router/routes';

import styles from './MainMenu.module.css';

const BUTTON_CLASS_NAME = 'bp3-button'
const BUTTON_ACTIVE_CLASS_NAME = 'bp3-active';

const LOGOUT_BUTTON_LABEL = 'Logout';
const SETTINGS_BUTTON_LABEL = 'Settings';
const EMPLOYEES_BUTTON_LABEL = 'Employees';
const PROJECTS_BUTTON_LABEL = 'Projects';
const EMPLOYERS_BUTTON_LABEL = 'Employers';
const WORKSHEETS_BUTTON_LABEL = 'Worksheets';

export interface IProps {}

export default function MainMenu(_props: IProps) {

	const Link = ({to, icon, children}: {
		to: string,
		icon: IconName,
		children: React.ReactNode,
	}) => (
		<NavLink
			className={BUTTON_CLASS_NAME}
			activeClassName={BUTTON_ACTIVE_CLASS_NAME}
			to={to}
			exact
		>
			<Icon icon={icon}/>
			<span>{children}</span>
		</NavLink>
	);

	return (
		<div className={styles.container}>
			<div className={styles.buttonGroup}>
				<Link icon="people" to={EMPLOYEES_PATH}>{EMPLOYEES_BUTTON_LABEL}</Link>
				<Link icon="projects" to={PROJECTS_PATH}>{PROJECTS_BUTTON_LABEL}</Link>
				<Button icon="build">{EMPLOYERS_BUTTON_LABEL}</Button>
				<Button icon="document">{WORKSHEETS_BUTTON_LABEL}</Button>
			</div>
			<div className={styles.buttonGroup}>
				<Link icon="log-out" to={ROOT_PATH}>{LOGOUT_BUTTON_LABEL}</Link>
				<Button icon="settings">{SETTINGS_BUTTON_LABEL}</Button>
			</div>
		</div>
	);
}
