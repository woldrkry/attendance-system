import React, { useState } from 'react';
import { Select, MenuItem } from '@material-ui/core';
import { connect } from 'react-redux';
import { IState } from '../../reducer';
import { getEmployeesMap } from '../../Employees/helper';
import { getProjectAssignmentsMapByContractIds, getEmployeeAssignments, getEmployeeProjects, getProjectsAssignments } from '../../Projects/helper';

export interface IOwnProps {
	employeeId: number;
	onSelect: (selectedProjectId: number) => void;
}

export interface IStateProps {
	projects: {
		id: number;
		name: string;
	}[];
}

export type IProps = IStateProps & IOwnProps;

export function EmployeeProjectsSelect(props: IProps) {
	const [ projectId, setProjectId ] = useState<number | ''>('');

	const { onSelect, projects } = props;

	const onChange = (
		event: React.ChangeEvent<{ name?: string; value: unknown }>,
		_child: React.ReactNode,
	) => {
		const selectedProjectId = event.target.value as number;
		setProjectId(selectedProjectId);
		onSelect(selectedProjectId);
	}

	const menuItems = projects.map(({id, name}) => <MenuItem value={id} key={id}>{name}</MenuItem>);

	return (
		<Select
			value={projectId}
			onChange={onChange}
			variant='filled'
		>
			{menuItems}
		</Select>
	);
}

export default connect(
	(state: IState, props: IOwnProps): IStateProps => {
		const { employeeId } = props;
		const employee = getEmployeesMap(state.employees.data)[employeeId];
		const projectsAssignments = getProjectsAssignments(Object.values(state.projects.data));
		const assignmentsMapByContractId = getProjectAssignmentsMapByContractIds(projectsAssignments);
		const employeeAssignments = employee && employee.contracts ? getEmployeeAssignments(assignmentsMapByContractId, employee.contracts) : [];
		const employeeProjects = getEmployeeProjects(employeeAssignments, state.projects.data);
		
		return {
			projects: Object.values(employeeProjects),
		}
	},
)(EmployeeProjectsSelect)
