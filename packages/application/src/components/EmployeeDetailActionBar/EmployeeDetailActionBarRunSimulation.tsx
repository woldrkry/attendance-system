import React, { useState, MouseEventHandler } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';
import { Button, Select, MenuItem, TextField } from '@material-ui/core';

import styles from './EmployeeDetailActionBar.module.css';
import { ProjectAssignment } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';
import { getEmployeesMap } from '../../Employees/helper';
import { getContractsMap } from '../../Contract/helper';
import { EmployeeSimulationRun } from '../../Employees/employeesThunk';
import { RecordsLoad } from '../../Records/recordsThunk';

const SELECT_PROJECT_ASSIGNMENT_LABEL = 'Úvazek k projektu';
const SELECT_MONTH_LABEL = 'Měsíc';
const INPUT_YEAR_LABEL = 'Rok';
const BUTTON_RUN_SIMULATION_LABEL = 'Spustit simulaci';

const MONTHS = moment.months();
const today = new Date();
const DEFAULT_MONTH = today.getMonth();
const DEFAULT_YEAR = today.getFullYear();

export interface IStateProps {
	projectAssignments: ProjectAssignment[],
}

export interface IDispatchProps {
	runSimulation: (
		employeeId: number,
		projectAssignmentId: number, 
		from: Date, 
		to: Date,
	) => void;
}

export interface IOwnProps {
	employeeId: number | undefined;
	projectId: number | undefined;
}

export type IProps = IOwnProps & IDispatchProps & IStateProps;

export function EmployeeDetailActionBarRunSimulation(props: IProps) {
	const { employeeId, projectId, runSimulation, projectAssignments } = props;
	const [ selectedMonth, setSelectedMonth ] = useState<number>(DEFAULT_MONTH);
	const [ selectedYear, setSelectedYear ] = useState<number>(DEFAULT_YEAR);
	const [ selectedProjectAssignmentIndex, setSelectedProjectAssignmentIndex ] = useState<number>(0);

	const handleMonthSelect = (
		event: React.ChangeEvent<{ name?: string; value: unknown }>,
		_child: React.ReactNode,
	) => {
		setSelectedMonth(event.target.value as number);
	};
	const itemsMonthSelect = MONTHS.map(
		(month, index) => <MenuItem value={index} key={index}>{month}</MenuItem>
	);

	const handleChangeYearInput = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSelectedYear(parseInt(event.target.value))
	};

	const disabledRunSimulationButton = employeeId === undefined
		|| projectId === undefined
		|| projectAssignments[selectedProjectAssignmentIndex] === undefined;
	const handleRunSimulationButton = () => {
		const from = moment.utc(`${selectedYear}-${selectedMonth+1}`).toDate();
		console.log(from);
		const to = moment(from).add(1, 'month').toDate();

		if (!disabledRunSimulationButton
			&& employeeId
			&& projectAssignments[selectedProjectAssignmentIndex]) {
			runSimulation(
				employeeId,
				projectAssignments[selectedProjectAssignmentIndex].id,
				from,
				to,
			);
		}
	};

	const handleProjectAssignmentSelect = (
		event: React.ChangeEvent<{ name?: string; value: unknown }>,
		_child: React.ReactNode,
	) => {
		setSelectedProjectAssignmentIndex(event.target.value as number);
	};
	const itemsProjectAssignmentSelect = projectAssignments.map(
		(projectAssignment, index) => <MenuItem value={index} key={index}>
			{`${projectAssignment.position ? projectAssignment.position + ' ' : ''}[${projectAssignment.from} - ${projectAssignment.to}] [${projectAssignment.extent}]`}
		</MenuItem>
	);

	return (
		<div className={styles.actionRow}>
			<Select
				label={SELECT_MONTH_LABEL}
				value={selectedMonth}
				onChange={handleMonthSelect}
				variant={'filled'}
			>
				{itemsMonthSelect}
			</Select>
			<TextField
				label={INPUT_YEAR_LABEL}
				type={'number'}
				InputLabelProps={{
					shrink: true,
				}}
				variant="filled"
				value={selectedYear}
				onChange={handleChangeYearInput}
			/>
			<Select
				label={SELECT_PROJECT_ASSIGNMENT_LABEL}
				value={selectedProjectAssignmentIndex}
				onChange={handleProjectAssignmentSelect}
				variant={'filled'}
			>
				{itemsProjectAssignmentSelect}
			</Select>
			<Button
				disabled={disabledRunSimulationButton}
				onClick={handleRunSimulationButton}
			>
				{BUTTON_RUN_SIMULATION_LABEL}
			</Button>
		</div> 
	);
}

export default connect(
	(state: IState, props: IOwnProps): IStateProps => {
		const employeesMap = getEmployeesMap(state.employees.data); 
		const contracts = props.employeeId 
			? employeesMap[props.employeeId]?.contracts
			: [];
		const contractsMap = contracts ? getContractsMap(contracts) : {};

		const filteredProjectAssignments: ProjectAssignment[] = [];
		const projectAssignments = props.projectId
			? state.projects.data[props.projectId]?.projectAssignments
			: [];

		projectAssignments.forEach((assignment) => {
			if (contractsMap[assignment.contractId]) {
				filteredProjectAssignments.push(assignment);
			}
		});
		return {
			projectAssignments: filteredProjectAssignments,
		};
	},
	(dispatch: ThunkDispatch): IDispatchProps => {
		return {
			runSimulation: (employeeId: number, projectAssignmentId: number, from: Date, to: Date) => {
				dispatch(EmployeeSimulationRun(employeeId, projectAssignmentId, from, to));
			},
		};
	},
)(EmployeeDetailActionBarRunSimulation);
 