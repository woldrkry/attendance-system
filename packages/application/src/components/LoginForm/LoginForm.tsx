import React from 'react';
import styles from './LoginForm.module.css';
import { Button, InputGroup } from '@blueprintjs/core';
import { Link } from 'react-router-dom';
import { EMPLOYEES_PATH } from '../../Router/routes';

const LOGIN_BUTTON_LABEL = 'Login';
const USERNAME_INPUT_PLACEHOLDER = 'Username...';
const PASSWORD_INPUT_PLACEHOLDER = 'Password...';


export interface IProps {}

export default function LoginForm(_props: IProps) {
	const containerClassName = styles.container;

	return (
		<div className={containerClassName}>
			<InputGroup type="text" placeholder={USERNAME_INPUT_PLACEHOLDER} />
			<InputGroup type="password" placeholder={PASSWORD_INPUT_PLACEHOLDER} />
			<Link to={EMPLOYEES_PATH}>
				<Button icon="log-in">{LOGIN_BUTTON_LABEL}</Button>
			</Link>
			
		</div>
	);
}
