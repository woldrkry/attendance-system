import React from 'react';
import _ from 'lodash';
import MaterialTable, { Column } from 'material-table';
import { connect } from 'react-redux';
import { ContractType } from 'domain-model/dist/model/Contract/ContractType';
import { IState } from '../../reducer';
import { ThunkDispatch } from '../../store';
import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { ProjectsLoad } from '../../Projects/projectsThunk';
import { EmployeesLoad } from '../../Employees/employeesThunk';

const TITLE_LABEL = 'Zaměstnanci projektu';
const EMPLOYEE_NAME_LABEL = 'Jméno';
const CONTRACT_TYPE_LABEL = 'Druh zaměstnance';
const ASSIGNMENT_EXTENT_LABEL = 'Velikost úvazku';
const EMPLOYEE_HOURS_LABEL = 'Odpracované hodiny';
const FROM_DATE_LABEL = 'Datum zahájení';
const TO_DATE_LABEL = 'Datum ukončení';

export interface I_ROW {
	id: number;
	employeeName: string;
	contractType: ContractType;
	assignmentExtent: number;
	hours: number;
	from: Date;
	to: Date;
}

const getColumns = (): Column<I_ROW>[] => {
	return [
		{ title: EMPLOYEE_NAME_LABEL, field: 'employeeName' },
		{ title: CONTRACT_TYPE_LABEL, field: 'contractType' },
		{ title: ASSIGNMENT_EXTENT_LABEL, field: 'assignmentExtent' },
		{ title: EMPLOYEE_HOURS_LABEL, field: 'hours' },
		{ title: FROM_DATE_LABEL, field: 'from', type: 'date' },
		{ title: TO_DATE_LABEL, field: 'to', type: 'date' },
	];
};

export interface IOwnProps {
	id: number;
}

export interface IStateProps {
	data: I_ROW[];
}

export interface IDispatchProps {}

export type IProps = IStateProps;

export function ProjectDetailEmployeesTable(props: IProps) {
	const { data } = props;
	const columns = getColumns();

	return (
		<MaterialTable<I_ROW>
			title={TITLE_LABEL}
			columns={columns}
			data={data}
		/>
	);
}

export default connect(
	(state: IState, props: IOwnProps): IStateProps => {
		const { id } = props;
		const employees = state.employees.data;
		const assignments = state.projects.data[id] ? state.projects.data[id].projectAssignments : [];
		const employeesMapByContractId: {[id: number ]: EmployeeAttributes} = {};
		employees.map((employee) => {
			employee.contracts?.forEach((contract) => {
				if (contract.id) {
					employeesMapByContractId[contract.id] = employee;
				}
			});
		});

		const projectEmployees = assignments.map((assignment): I_ROW | undefined => {
			const contractId = assignment.contractId;
			const employee = employeesMapByContractId[contractId] ? employeesMapByContractId[contractId] : undefined ;
			if (employee) {
				const employeeContract = employee?.contracts?.find((contract) => contract.id === contractId);
				return {
					id: employee.id!,
					employeeName: employee.name,
					contractType: employeeContract!.type,
					assignmentExtent: assignment.extent,
					hours: 0, //TODO:
					from: new Date(assignment.from),
					to: new Date(assignment.to),
				};
			} else {
				return undefined;
			}
		});

		const projectEmployeesFiltered = _.without(projectEmployees, undefined) as I_ROW[];

		return {
			data: projectEmployeesFiltered,
		};
	},
	(dispatch: ThunkDispatch): IDispatchProps => {
		dispatch(EmployeesLoad());
		dispatch(ProjectsLoad());
		return {};
	},
)(ProjectDetailEmployeesTable);
