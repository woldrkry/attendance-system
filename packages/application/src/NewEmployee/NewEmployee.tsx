import React from 'react';
import EmployeeForm from '../components/EmployeeForm/EmployeeForm';
import { ProjectsMap } from '../Projects/helper';
import { IState } from '../reducer';
import { ThunkDispatch } from '../store';
import { ProjectsLoad } from '../Projects/projectsThunk';
import { connect } from 'react-redux';
import EmployeeLocalAttributes from '../Employees/EmployeeLocalAttributes';
import { NewEmployeeDetailChange, NewEmployeeContractChange, NewEmployeeSimulationChange, NewEmployeeProjectAssignmentChange, NewEmployeeProjectAssignmentCreate, NewEmployeeContractCreate, NewEmployeeContractRemove, NewEmployeeProjectAssignmentRemove, NewEmployeeCreateResetForm } from './newEmployeeActions';
import { NewEmployeeSave } from './newEmployeeThunk';
import SimulationAttributes from 'domain-model/dist/model/Simulation/SimulationAttributes';
import { ContractToCreate, Contract } from 'domain-model/dist/model/Contract/ContractAttributes';
import { ProjectAssignment, ProjectAssignmentToCreate } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

interface IOwnProps {}

interface IStateProps {
	availableProjectsMap: ProjectsMap;
	academicTitle: string | undefined | null;
	dateOfBirth: Date;
	name: string | undefined;
	simulation: Partial<SimulationAttributes>;
	contracts: (Partial<Contract> & { id: number })[];
	projectAssignments: Partial<ProjectAssignment>[];
	errorMessages: string[];
	created: boolean;
}

interface IDispatchProps {
	resetForm: () => void;
	saveEmployee: (data: Partial<Pick<EmployeeLocalAttributes, 'name' | 'academicTitle' | 'dateOfBird'>>) => void;
	saveSimulationLocally: (data: Partial<SimulationAttributes>) => void;
	saveContractLocally: (index: number, data: Partial<ContractToCreate>) => void;
	createContractLocally: (data: Partial<ContractToCreate>) => void;
	saveProjectAssignmentLocally: (index: number, data: Partial<ProjectAssignment>) => void;
	createProjectAssignmentLocally: (data: Partial<ProjectAssignmentToCreate>) => void;
	createEmployee: () => void;
	removeContractLocally: (id: number) => void;
	removeProjectAssignmentLocally: (id: number) => void;
}

export type IProps = IOwnProps & IStateProps & IDispatchProps;

function NewEmployee(props: IProps) {
	const {
		availableProjectsMap,
		saveEmployee,		
	} = props;

	return (
		<div>
			<EmployeeForm
				availableProjectsMap={availableProjectsMap}
				academicTitle={props.academicTitle || ''}
				dateOfBird={props.dateOfBirth}
				name={props.name || ''}
				projectsAssignments={props.projectAssignments}
				saveContractLocally={props.saveContractLocally}
				createContractLocally={props.createContractLocally}
				saveEmployeeLocally={saveEmployee}
				saveProjectAssignmentLocally={props.saveProjectAssignmentLocally}
				createProjectAssignmentLocally={props.createProjectAssignmentLocally}
				saveRemote={props.createEmployee}
				saveSimulationLocally={props.saveSimulationLocally}
				contracts={props.contracts}
				simulation={props.simulation}
				removeContractLocally={props.removeContractLocally}
				removeProjectAssignmentLocally={props.removeProjectAssignmentLocally}
				errorMessages={props.errorMessages}
				created={props.created}
				resetForm={props.resetForm}
			/>
		</div>
	);
}


export default connect(
	(state: IState, ownProps: IOwnProps): IStateProps => ({
		availableProjectsMap: state.projects.data,
		academicTitle: state.newEmployee.employee.academicTitle,
		dateOfBirth: state.newEmployee.employee.dateOfBird ? new Date(state.newEmployee.employee.dateOfBird as any) : new Date(),
		name: state.newEmployee.employee.name,
		simulation: state.newEmployee.simulation,
		contracts: Object.values(state.newEmployee.contracts),
		projectAssignments: state.newEmployee.projectsAssignments,
		errorMessages: state.newEmployee.errorMessages,
		created: state.newEmployee.created,
	}),
	(dispatch: ThunkDispatch): IDispatchProps => {
		dispatch(ProjectsLoad());
		return {
			saveEmployee: (data: Partial<Pick<EmployeeLocalAttributes, 'name' | 'academicTitle' | 'dateOfBird'>>) => dispatch<NewEmployeeDetailChange>({
				type: NewEmployeeDetailChange,
				employee: data,
			}),
			saveSimulationLocally: (data: Partial<SimulationAttributes>) => dispatch<NewEmployeeSimulationChange>({
				type: NewEmployeeSimulationChange,
				simulation: data,
			}),
			saveContractLocally: (id: number, data: Partial<ContractToCreate>) => dispatch<NewEmployeeContractChange>({
				type: NewEmployeeContractChange,
				id,
				contract: data,
			}),
			createContractLocally: (data: Partial<ContractToCreate>) => dispatch<NewEmployeeContractCreate>({
				type: NewEmployeeContractCreate,
				contract: data,
			}),
			saveProjectAssignmentLocally: (index: number, data: Partial<ProjectAssignment>) => dispatch<NewEmployeeProjectAssignmentChange>({
				type: NewEmployeeProjectAssignmentChange,
				index: index,
				projectAssignment: data,
			}),
			createProjectAssignmentLocally: (data: Partial<ProjectAssignmentToCreate>) => dispatch<NewEmployeeProjectAssignmentCreate>({
				type: NewEmployeeProjectAssignmentCreate,
				projectAssignment: data,
			}),
			removeContractLocally: (id: number) => dispatch<NewEmployeeContractRemove>({
				type: NewEmployeeContractRemove,
				id,
			}),
			removeProjectAssignmentLocally: (index: number) => dispatch<NewEmployeeProjectAssignmentRemove>({
				type: NewEmployeeProjectAssignmentRemove,
				index,
			}),
			createEmployee: () => dispatch(NewEmployeeSave()),
			resetForm: () => dispatch<NewEmployeeCreateResetForm>({
				type: NewEmployeeCreateResetForm,
			}),
		};
	},
)(NewEmployee);
