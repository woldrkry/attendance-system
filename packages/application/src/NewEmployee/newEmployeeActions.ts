import EmployeeLocalAttributes from '../Employees/EmployeeLocalAttributes';
import EmployeeAttributes, { Employee } from 'domain-model/dist/model/Employee/EmployeeAttributes';
import SimulationAttributes from 'domain-model/dist/model/Simulation/SimulationAttributes';
import { ContractAttributes, ContractToCreate } from 'domain-model/dist/model/Contract/ContractAttributes';
import { ProjectAssignmentAttributes, ProjectAssignment, ProjectAssignmentToCreate } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

export const NewEmployeeDetailChange = 'NewEmployee.Detail.Change';
export interface NewEmployeeDetailChange {
	type: typeof NewEmployeeDetailChange;
	employee: Partial<Pick<EmployeeLocalAttributes, 'name' | 'academicTitle' | 'dateOfBird'>>;
}

export const NewEmployeeSimulationChange = 'NewEmployee.Simulation.Change';
export interface NewEmployeeSimulationChange {
	type: typeof NewEmployeeSimulationChange;
	simulation: Partial<SimulationAttributes>;
}

export const NewEmployeeContractChange = 'NewEmployee.Contract.Change';
export interface NewEmployeeContractChange {
	type: typeof NewEmployeeContractChange;
	id: number;
	contract: Partial<ContractToCreate>;
}

export const NewEmployeeContractCreate = 'NewEmployee.Contract.Create';
export interface NewEmployeeContractCreate {
	type: typeof NewEmployeeContractCreate;
	contract: Partial<ContractToCreate>;
}

export const NewEmployeeContractRemove = 'NewEmployee.Contract.Remove';
export interface NewEmployeeContractRemove {
	type: typeof NewEmployeeContractRemove;
	id: number;
}

export const NewEmployeeProjectAssignmentChange = 'NewEmployee.ProjectAssignment.Change';
export interface NewEmployeeProjectAssignmentChange {
	type: typeof NewEmployeeProjectAssignmentChange;
	index: number;
	projectAssignment: Partial<ProjectAssignment>;
}

export const NewEmployeeProjectAssignmentCreate = 'NewEmployee.ProjectAssignment.Create';
export interface NewEmployeeProjectAssignmentCreate {
	type: typeof NewEmployeeProjectAssignmentCreate;
	projectAssignment: Partial<ProjectAssignmentToCreate>;
}

export const NewEmployeeProjectAssignmentRemove = 'NewEmployee.ProjectAssignment.Remove';
export interface NewEmployeeProjectAssignmentRemove {
	type: typeof NewEmployeeProjectAssignmentRemove;
	index: number;
}

export const NewEmployeeCreate = 'NewEmployee.Create';
export interface NewEmployeeCreate {
	type: typeof NewEmployeeCreate;
	response: Employee;
}

export const NewEmployeeCreateValidationError = 'NewEmployee.Create.Validation.Error';
export interface NewEmployeeCreateValidationError {
	type: typeof NewEmployeeCreateValidationError;
	errorsMessages: string[];
}

export const NewEmployeeCreateResetForm = 'NewEmployee.Create.Reset.Form';
export interface NewEmployeeCreateResetForm {
	type: typeof NewEmployeeCreateResetForm;
}

type NewEmployeeActions = NewEmployeeDetailChange
	| NewEmployeeSimulationChange
	| NewEmployeeContractChange
	| NewEmployeeContractCreate
	| NewEmployeeContractRemove
	| NewEmployeeProjectAssignmentChange
	| NewEmployeeProjectAssignmentCreate
	| NewEmployeeProjectAssignmentRemove
	| NewEmployeeCreate
	| NewEmployeeCreateValidationError
	| NewEmployeeCreateResetForm;

export default NewEmployeeActions;
