import { ThunkAction } from '../store';
import { WorksheetCreateRequest, WorksheetCreateError, WorksheetCreateSuccess, WorksheetCreateAction, WorksheetCreateCancel } from './worksheetsActions';
import { doPost } from '../Api/doRequest';
import { getEmployeesMap } from '../Employees/helper';

export const GenerateWorksheet = (
	employeeId: number,
	projectId: number,
	from: Date,
	to: Date,
): ThunkAction => async (dispatch, getState) => {
	const state = getState();
	const { api } = state;
	const apiBaseUrl = api.baseUrl;
	const resource = 'employees';
	const secondResource = 'worksheets'
	const url = `${apiBaseUrl}/${resource}/${employeeId}/${secondResource}`;

	const worksheetCreateActionId: WorksheetCreateAction = { employeeId, projectId, from, to, requestedAt: new Date() };

	const employeeName = (getEmployeesMap(state.employees.data))[employeeId]!.name;
	const projectName = state.projects.data[projectId].name;
	const defaultFileName = `Worksheet_${employeeName}_${projectName}_${from.getFullYear()}_${to.getFullYear()}.pdf`;

	try {
		dispatch<WorksheetCreateRequest>({
			...worksheetCreateActionId,
			type: WorksheetCreateRequest,
		});

		const dialogResponse = await openDialogSaveFile(defaultFileName);
		console.log('dialogResponse', dialogResponse);
		if (dialogResponse.canceled) {
			dispatch<WorksheetCreateCancel>({
				...worksheetCreateActionId,
				type: WorksheetCreateCancel,
			});
		} else {
			const path = dialogResponse.filePath;

			console.log(path);

			const worksheet = await doPost<ArrayBuffer, any>(
				url,
				{
					employeeId,
					projectId,
					from: from.toISOString(),
					to: to.toISOString(),
				},
				true,
			);

			await writeFileSystemFile(path, worksheet);

			dispatch<WorksheetCreateSuccess>({
				...worksheetCreateActionId,
				type: WorksheetCreateSuccess,
			});
		}
	} catch (error) {
		dispatch<WorksheetCreateError>({
			...worksheetCreateActionId,
			type: WorksheetCreateError,
			message: error.message,
		});
	}
};

export function openDialogSaveFile(
	defaultFileName: string,
): Promise<{ canceled: true, filePath: undefined } | { canceled: false, filePath: string }> {
	return new Promise((resolve) => {
		window.ipcRenderer.send('Dialog.Save.File.Request', { defaultFileName });

		window.ipcRenderer.on('Dialog.Save.File.Succeed', (_, data) => {
			console.log(data);
			resolve(data);
		});
	});
}

export function writeFileSystemFile(path: string, data: any): Promise<void> {
	return new Promise((resolve, reject) => {
		window.ipcRenderer.send('FileSystem.File.Write.Request', { filePath: path, data: data });

		window.ipcRenderer.on('FileSystem.File.Write.Succeed', (_, data) => {
			console.log('write file success', data);
			resolve();
		});

		window.ipcRenderer.on('FileSystem.File.Write.Failed', (_, data) => {
			console.log('write file fail', data);
			reject();
		});
	});
}
