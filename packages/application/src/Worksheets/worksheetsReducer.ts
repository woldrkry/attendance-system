import WorksheetsActions, { WorksheetCreateRequest, WorksheetCreateSuccess,  } from './worksheetsActions';
import LOAD_STATE from '../LoadState/LoadState';

export interface IWorksheetsState {
	state: LOAD_STATE;
	error: string | undefined;
}

const initState: IWorksheetsState = {
	state: LOAD_STATE.INITIAL,
	error: undefined,
};

export default function (state: IWorksheetsState = initState, action: WorksheetsActions) {

	switch (action.type) {
		case WorksheetCreateRequest:
			return {
				...state,
				state: LOAD_STATE.LOADING,
			};

		case WorksheetCreateSuccess:
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
			};
	
		default:
			return state;
	}
}
