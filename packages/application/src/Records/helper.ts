import { Record } from 'domain-model/dist/model/Record/RecordAttributes';

export type RecordsMap = {[key: number]: Record};
