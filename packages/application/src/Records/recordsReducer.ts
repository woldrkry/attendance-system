
import { RecordsMap } from './helper';
import LOAD_STATE from '../LoadState/LoadState';
import RecordsActions, {
	RecordsLoadRequest,
	RecordsLoadError,
	RecordsLoadSuccess,
	NewRecordCreateSuccess,
	RecordUpdateSuccess,
	RecordDeleteSuccess,
} from './recordsActions';
import { deepJSONCopy } from '../helper';

export interface IRecordsState {
	data: RecordsMap;
	state: LOAD_STATE;
	error: string | undefined;
}

const initState: IRecordsState = {
	data: {},
	state: LOAD_STATE.INITIAL,
	error: undefined,
}

export default function (state: IRecordsState = initState, action: RecordsActions): IRecordsState {

	switch (action.type) {
		case RecordsLoadRequest:
			return {
				...state,
				state: LOAD_STATE.LOADING,
			};

		case RecordsLoadSuccess:
			const recordsMap = action.response.reduce<RecordsMap>((prev, current) => {
				prev[current.id] = current;
				return prev;
			}, {});
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				data: recordsMap,
			};

		case RecordsLoadError:
			return {
				...state,
				state: LOAD_STATE.SUCCESS,
				error: action.message,
			};

		case NewRecordCreateSuccess:
		case RecordUpdateSuccess:
			return {
				...state,
				data: {
					...(deepJSONCopy<RecordsMap>(state.data)),
					[action.data.id]: action.data,
				},
			};

		case RecordDeleteSuccess:
			const recordsMapDeleted = deepJSONCopy<RecordsMap>(state.data);
			delete recordsMapDeleted[action.data.id];
			return {
				...state,
				data: recordsMapDeleted,
			}
	
		default:
			return state;
	}
}