import EmployeeAttributes from 'domain-model/dist/model/Employee/EmployeeAttributes';
import { ProjectAssignmentAttributes } from 'domain-model/dist/model/Project/ProjectAssignmentAttributes';

export default interface EmployeeLocalAttributes extends EmployeeAttributes {
		projectsAssignments: ProjectAssignmentAttributes[];
}
