import { Router, Request, Response, NextFunction } from 'express';
import { OK, CREATED, NOT_FOUND } from 'http-status';
import ContractModel from 'domain-model/dist/model/Contract/ContractModel';

export const contracts = Router();

contracts.post('/:employeeId/contracts', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { body } = req;
		const employeeId = req.params.employeeId;

		const contractToCreate = { ...body,  employeeId };
		const contract = await ContractModel.create(contractToCreate);
		res.status(CREATED).send(contract);
	} catch (error) {
		next(error);
	}
});

contracts.get('/:employeeId/contracts', async (req: Request, res: Response, next: NextFunction) => {
	try {
		const employeeId = req.params.employeeId;
		const contract = await ContractModel.findAll({where: { employeeId }});
		if (contract) {
			res.status(OK).send(contract);
		} else {
			res.status(NOT_FOUND).send();
		}
	} catch (error) {
		next(error);
	}
});
