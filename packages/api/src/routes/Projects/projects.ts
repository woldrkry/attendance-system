import { Router } from 'express';
import { OK, CREATED } from 'http-status';
import ProjectModel from 'domain-model/dist/model/Project/ProjectModel';
import { projectAssignments } from './ProjectAssignments/projectAssignments';
import ProjectAssignmentModel from 'domain-model/dist/model/Project/ProjectAssignmentModel';

export const projects = Router();

projects.post('/', async (req, res, next) => {
	try {
		const project = await ProjectModel.create(req.body);
		res.status(CREATED).send(project);
	} catch (error) {
		next(error);
	}
});

projects.get('/', async (req, res, next) => {
	try {
		const projects = await ProjectModel.findAll({ include: [ProjectAssignmentModel] });
		res.status(OK).send(projects);
	} catch (e) {
		next(e);
	}
});

projects.use('/', projectAssignments);
