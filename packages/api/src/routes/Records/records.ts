import { Router } from 'express';
import { OK, CREATED } from 'http-status';
import RecordModel from 'domain-model/dist/model/Record/RecordModel';

export const records = Router();

records.post('/', async (req, res, next) => {
	try {
		const record = await RecordModel.create(req.body);
		res.status(CREATED).send(record);
	} catch (error) {
		next(error);
	}
});

records.put('/', async (req, res, next) => {
	try {
		const recordToUpdate = { ...req.body };
		delete recordToUpdate.id;
		await RecordModel.update(
			recordToUpdate,
			{where: {id: req.body.id}},
		);
		const record = await RecordModel.findOne({where: {id: req.body.id}});
		res.status(OK).send(record);
	} catch (error) {
		next(error);
	}
});

records.delete('/', async (req, res, next) => {
	try {
		const record = await RecordModel.findOne({where: {id: req.body.id}});
		await RecordModel.destroy({where: {id: req.body.id}});
		res.status(OK).send(record);
	} catch (error) {
		next(error);
	}
});

records.get('/', async (req, res, next) => {
	try {
		const records = await RecordModel.findAll();
		res.status(OK).send(records);
	} catch (e) {
		next(e);
	}
});
