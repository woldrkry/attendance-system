import DatesRecordsMap from './DatesRecordsMap';
import { RecordAttributes } from 'domain-model/dist/model/Record/RecordAttributes';


export default function mapRecordsToDates(records: RecordAttributes[]): DatesRecordsMap {
	const map: DatesRecordsMap = new DatesRecordsMap(records);

	return map;
}
