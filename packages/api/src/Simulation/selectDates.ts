import DatesRecordsMap from './DatesRecordsMap';
import _ = require('lodash');
import { selectRandomInteger } from './selectors';

export interface ISelectDatesOptions {
	from: Date;
	to: Date;
	recordsMap: DatesRecordsMap;
	workingDays: number[];
	minutes: number;
	workingHoursLengthInMinutes: number;
}

export default function selectDates(options: ISelectDatesOptions): Date[] {
	const preSelected: Date[] = createWorkingDates(options);
	const selected: Date[] = selectDatesBySelector(
		preSelected,
		options.recordsMap,
		options.minutes,
		options.workingHoursLengthInMinutes,
	);

	return selected;
}

export function createWorkingDates(options: ISelectDatesOptions): Date[] {
	const { from, to, workingDays } = options;
	const selected: Date[] = [];

	let current = _.cloneDeep(from);
	while (current < to) {
		const isCurrentWorkingDay = isWorkingDay(workingDays, current);
		if (isCurrentWorkingDay) {
			selected.push(current);
		}

		current = _.cloneDeep(current);
		current.setDate(current.getDate() + 1);
	}

	return selected;
}

export function isWorkingDay(workingDays: number[], date: Date): boolean {
	return workingDays.includes(date.getDay());
}

export function selectDatesBySelector(
	dates: Date[],
	recordsMap: DatesRecordsMap,
	minutesToSelect: number,
	workingDayHoursLengthInMinutes: number,
	minDayFreeMinutes: number = 30,
	selector: (max: number) => number = selectRandomInteger,
): Date[] {
	let leftMinutes: number = minutesToSelect;
	let leftDates: Date[] = _.cloneDeep(dates);
	const selectedDates: Date[] = [];
	
	while (leftDates.length > 0 && leftMinutes > 0) {
		const selectedLeftDateIndex = selector(leftDates.length-1);
		const selectDate = leftDates[selectedLeftDateIndex];
		const selectDateRecords = recordsMap.get({
			fullYear: selectDate.getFullYear(),
			month: selectDate.getMonth(),
			date: selectDate.getDate(),
		});

		const minutesRemainingInDay = workingDayHoursLengthInMinutes - selectDateRecords.minutes;
		if (minutesRemainingInDay >= minDayFreeMinutes) {
			selectedDates.push(selectDate);
			leftMinutes -= minutesRemainingInDay;
		}

		leftDates.splice(selectedLeftDateIndex, 1);
	}

	return selectedDates;
}
