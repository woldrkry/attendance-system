
export function selectRandomInteger(max: number, min: number = 0): number {
	return Math.floor(Math.random() * Math.floor(max - min)) + Math.floor(min);
}
