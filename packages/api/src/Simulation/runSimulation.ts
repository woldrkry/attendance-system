import * as _ from 'lodash';
import EmployeeModel from 'domain-model/dist/model/Employee/EmployeeModel';
import SimulationModel from 'domain-model/dist/model/Simulation/SimulationModel';
import ProjectAssignmentModel from 'domain-model/dist/model/Project/ProjectAssignmentModel';
import selectDates from './selectDates';
import mapRecordsToDates from './mapRecordsToDates';
import { RecordToCreate, RecordAttributes } from 'domain-model/dist/model/Record/RecordAttributes';
import getRecordsByProjectIdAndEmployeeId from '../Records/getRecordsByProjectIdAndEmployeeId';
import findContractByProjectAssignmentId from '../Contracts/findContractByProjectAssignmentId';
import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import { selectRandomInteger } from './selectors';

export interface ISimulationOptions {
	employeeId: number;
	projectAssignmentId: number;
	from: Date;
	to: Date;
}

export default async function runSimulation(options: ISimulationOptions) {
	const { employeeId, projectAssignmentId, from, to } = options;

	const employee = await EmployeeModel.findOne({where: { id: employeeId }, include: [SimulationModel]});
	const projectAssignment = await ProjectAssignmentModel.findOne({where: { id: projectAssignmentId }});
	const contract = await findContractByProjectAssignmentId(projectAssignmentId);
	const records: RecordAttributes[] = projectAssignment?.projectId
		? await getRecordsByProjectIdAndEmployeeId(
			projectAssignment.projectId,
			employeeId,
			from,
			to,
		): [];

	if (employee === null) {
		throw new Error(`Employee with id ${employeeId} not found`);
	}
	if (projectAssignment === null) {
		throw new Error(`Project assignment with id ${projectAssignmentId} not found`);
	}
	if (contract === null) {
		throw new Error(`Contract with project assignment id ${projectAssignmentId} not found`);
	}
	if (from < projectAssignment.from) {
		throw new Error(`Simulation can be run for date from ${from.toISOString()}. Project assignment is from ${projectAssignment.from.toISOString()}`);
	} 
	if (to > projectAssignment.to) {
		throw new Error(`Simulation can be run for date to ${to.toISOString()}. Project assignment is to ${projectAssignment.to.toISOString()}`);
	} 

	employee.simulationData
	
	const createdRecords = await runSimulationHelper(
		records,
		projectAssignment,
		employee,
		from,
		to,
	);

	await RecordModel.bulkCreate(createdRecords);
}

export async function runSimulationHelper(
	records: RecordAttributes[],
	projectAssignment: ProjectAssignmentModel,
	employee: EmployeeModel,
	from: Date,
	to: Date,
): Promise<RecordToCreate[]> {
	const employeeId = employee.id;
	const recordsMap = mapRecordsToDates(records);
	const usedMinutes = records.reduce((total, record) => {
		const recordFromInMinutes = getDateHoursAndMinutesInMinutes(record.from);
		const recordToInMinutes = getDateHoursAndMinutesInMinutes(record.to);
		return total + (recordToInMinutes - recordFromInMinutes);
	}, 0);
	const totalMinutes = projectAssignment.extent * 160 * 60;
	const minutes = totalMinutes - usedMinutes;

	const dates = selectDates({
		from,
		to,
		recordsMap,
		workingDays: JSON.parse(employee.simulationData.weekDaysJSONArray),
		minutes,
		workingHoursLengthInMinutes: employee.simulationData.workingHoursLengthMinutesFrom,
	});

	//TODO: check remaining time
	const createdRecords: RecordToCreate[] = [];
	dates.forEach((date) => {
		const { records: recordsInDate } = recordsMap.get({
			fullYear: date.getFullYear(),
			month: date.getMonth(),
			date: date.getDate(),
		});

		const recordsInDateExist = recordsInDate.length > 0;
		if (recordsInDateExist) {
			createdRecords.push(...createNewRecordsToFitExisting(
				date,
				employee.simulationData.workingHoursBeginningMinutesFrom,
				employee.simulationData.workingHoursLengthMinutesFrom,
				employeeId,
				projectAssignment.projectId,
				recordsInDate,
			));
		} else {
			const emptyDayWorkingHourBeginningMinutes = selectRandomInteger(
				employee.simulationData.workingHoursBeginningMinutesTo,
				employee.simulationData.workingHoursBeginningMinutesFrom,
			);
			createdRecords.push(...createNewRecordsForEmptyDate(
				date,
				emptyDayWorkingHourBeginningMinutes,
				employee.simulationData.workingHoursLengthMinutesFrom,
				employeeId,
				projectAssignment.projectId,
			));
		}
	});

	return createdRecords;
}

export function createNewRecordsForEmptyDate(
	date: Date,
	workingHoursBeginningInMinutes: number,
	workingHoursLengthInMinutes: number,
	employeeId: number,
	projectId: number,
): RecordToCreate[] {
	const from = new Date(
		date.getFullYear(),
		date.getMonth(),
		date.getDate(),
		0,
		workingHoursBeginningInMinutes,
	);
	const to = new Date(
		date.getFullYear(),
		date.getMonth(),
		date.getDate(),
		0,
		workingHoursBeginningInMinutes + workingHoursLengthInMinutes,
	);

	const record: RecordToCreate = {
		employeeId,
		projectId,
		description: 'General work on the project',
		type: 'office',
		from: from.toISOString(),
		to: to.toISOString(),
	};

	return [record];
}

export function createNewRecordsToFitExisting(
	_date: Date,
	workingHoursBeginningInMinutes: number,
	workingHoursLengthInMinutes: number,
	employeeId: number,
	projectId: number,
	existingRecords: RecordAttributes[],
): RecordToCreate[] {
	const recordsToReturn: RecordToCreate[] = [];
	const sortedExistingRecords = _.sortBy<RecordAttributes>(existingRecords, ['from']);
	const workingHoursEnd = workingHoursBeginningInMinutes + workingHoursLengthInMinutes;

	for (let index = 0; index < sortedExistingRecords.length; index++) {
		const prevRecord: RecordAttributes | null = index-1 >= 0 ? sortedExistingRecords[index-1] : null;
		const record: RecordAttributes = sortedExistingRecords[index];

		const prevRecordToInMinutes: number | null = prevRecord ? getDateHoursAndMinutesInMinutes(prevRecord.to) : null;
		const recordFromInMinutes = getDateHoursAndMinutesInMinutes(record.from);
		const recordToInMinutes = getDateHoursAndMinutesInMinutes(record.to);

		const isRecordFirst = index === 0;
		const isFreeTimeInBetween = prevRecordToInMinutes ? recordFromInMinutes > prevRecordToInMinutes + 1 : false;
		const isRecordLast = index === sortedExistingRecords.length-1;

		const isRecordFromAfterWorkingHoursBeginning = recordFromInMinutes > workingHoursBeginningInMinutes;
		const isRecordToBeforeWorkingHoursEnd = recordToInMinutes < workingHoursEnd;

		if (isRecordFirst &&
			isRecordFromAfterWorkingHoursBeginning) {
			recordsToReturn.push(createRecord(
				employeeId,
				projectId,
				record.from,
				workingHoursBeginningInMinutes,
				recordFromInMinutes-1,
			));
		}
		if (isFreeTimeInBetween &&
			prevRecordToInMinutes) {
			recordsToReturn.push(createRecord(
				employeeId,
				projectId,
				record.from,
				prevRecordToInMinutes+1,
				recordFromInMinutes-1,
			));
		}
		if (isRecordLast &&
			isRecordToBeforeWorkingHoursEnd) {
			recordsToReturn.push(createRecord(
				employeeId,
				projectId,
				record.from,
				recordToInMinutes+1,
				workingHoursEnd,
			));
		}
	}

	return recordsToReturn;
}

export function createRecord(
	employeeId: number,
	projectId: number,
	date: Date,
	fromInMinutes: number,
	toInMinutes: number,
): RecordToCreate {
	const recordBase: RecordToCreate = {
		employeeId,
		projectId,
		description: 'General work on the project',
		type: 'office',
		from: new Date(0).toISOString(),
		to: new Date(0).toISOString(),
	};
	return {
		...recordBase,
		from: new Date(
			date.getFullYear(),
			date.getMonth(),
			date.getDate(),
			0,
			fromInMinutes,
		).toISOString(),
		to: new Date(
			date.getFullYear(),
			date.getMonth(),
			date.getDate(),
			0,
			toInMinutes,
		).toISOString(),
	};
};

export function getDateHoursAndMinutesInMinutes(date: Date): number {
	return date.getHours()*60+date.getMinutes();
}
