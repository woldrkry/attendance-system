import { RecordAttributes } from 'domain-model/dist/model/Record/RecordAttributes';
import { getDateHoursAndMinutesInMinutes } from './runSimulation';

export interface DatesRecordsMapEntry {
	records: RecordAttributes[],
	minutes: number,
}

export default class DatesRecordsMap {
	private map: { [key: string]: DatesRecordsMapEntry | undefined } = {};

	public constructor(records?: RecordAttributes[]) {
		records?.forEach((record) => {
			this.set(record);
		});
	}

	public get(key: IDatesRecordsMapKey): DatesRecordsMapEntry {
		const emptyEntry: DatesRecordsMapEntry = {
			records: [],
			minutes: 0,
		}
		return this.map[JSON.stringify(key)] || emptyEntry;
	}

	public set(record: RecordAttributes): void {
		const key: IDatesRecordsMapKey = {
			fullYear: record.from.getFullYear(),
			month: record.from.getMonth(),
			date: record.from.getDate(),
		};

		const recordFromInMinutes = getDateHoursAndMinutesInMinutes(record.from);
		const recordToInMinutes = getDateHoursAndMinutesInMinutes(record.to);
		const recordLength = recordToInMinutes - recordFromInMinutes;

		const entry = this.map[JSON.stringify(key)];

		if (entry) {
			entry.records.push(record);
			entry.minutes += recordLength;
		} else {
			this.map[JSON.stringify(key)] = {
				records: [record],
				minutes: recordLength,
			};
		}
	}
}

export interface IDatesRecordsMapKey {
	fullYear: number,
	month: number, //JS month
	date: number,
}
