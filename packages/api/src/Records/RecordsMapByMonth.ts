import RecordModel from 'domain-model/dist/model/Record/RecordModel';

type RecordsMapByMonth = RecordModel[][];

export default RecordsMapByMonth;
