import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import { Op } from 'sequelize';

export default function getRecordsByProjectIdAndEmployeeId(
    projectId: number,
    employeeId: number,
    from: Date,
    to: Date,
): Promise<RecordModel[]> {
    return RecordModel.findAll({
        where: {
            projectId: projectId,
            employeeId,
            from: {
                [Op.gte]: from,
                [Op.lte]: to,
            },
        },
        order: [
            ['from', 'ASC'],
        ],
    })
}
