import RecordModel from 'domain-model/dist/model/Record/RecordModel';
import RecordsMapByDate from './RecordsMapByDate';


export default function mapRecordsToDate(records: RecordModel[]): RecordsMapByDate {
	const emptyMap: RecordsMapByDate = [];
	for (let index = 0; index < 31; index++) {
		emptyMap.push([]);
	}

	return records.reduce<RecordsMapByDate>(
		(map, record) => {
			map[record.from.getDate()-1].push(record);
			return map;
		},
		emptyMap,
	);
}
