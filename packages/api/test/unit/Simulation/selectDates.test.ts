import { isWorkingDay, selectDatesBySelector, createWorkingDates } from '../../../src/Simulation/selectDates';
import DatesRecordsMap from '../../../src/Simulation/DatesRecordsMap';

const createMockSelector = () => {
	let prevVal: number | undefined = undefined;
	return (max: number) => {
		const newValCandidate = prevVal ? prevVal + 1 : 1;
		const newVal = Math.floor(max < newValCandidate ? max : newValCandidate);
		prevVal = newVal;
		return newVal;
	};
};

describe('selectDates', () => {

	describe('createWorkingDates', () => {

		it('should select mondays in given period', () => {
			const expected: Date[] = [
				new Date('2020-06-01T00:00:00.000Z'),
				new Date('2020-06-08T00:00:00.000Z'),
				new Date('2020-06-15T00:00:00.000Z'),
				new Date('2020-06-22T00:00:00.000Z'),
				new Date('2020-06-29T00:00:00.000Z'),
			];
			const actual: Date[] = createWorkingDates({
				workingDays: [1] as number[],
				from: new Date('2020-06-01T00:00:00.000Z'),
				to: new Date('2020-06-30T00:00:00.000Z'),
			} as any);

			expect(actual).toEqual(expected);
		});

		it('should not select any days because from and to is the same date', () => {
			const expected: Date[] = [];
			const actual: Date[] = createWorkingDates({
				workingDays: [0, 1, 2, 3, 4, 5, 6] as number[],
				from: new Date('2020-06-01T00:00:00.000Z'),
				to: new Date('2020-06-01T00:00:00.000Z'),
			} as any);

			expect(actual).toEqual(expected);
		});

		it('should not select any days because there are non in given period', () => {
			const expected: Date[] = [];
			const actual: Date[] = createWorkingDates({
				workingDays: [0, 6] as number[],
				from: new Date('2020-06-01T00:00:00.000Z'),
				to: new Date('2020-06-05T00:00:00.000Z'),
			} as any);

			expect(actual).toEqual(expected);
		});

		it('should not select any days because no working days are given', () => {
			const expected: Date[] = [];
			const actual: Date[] = createWorkingDates({
				workingDays: [] as number[],
				from: new Date('2020-06-01T00:00:00.000Z'),
				to: new Date('2020-07-05T00:00:00.000Z'),
			} as any);

			expect(actual).toEqual(expected);
		});

	});

	describe('selectDateBySelector', () => {

		it('should select dates with given non-zero amount of free time', () => {
			const expected: Date[] = [
				new Date('2020-02-13T00:00:00.000Z'),
				new Date('2020-02-16T00:00:00.000Z'),
				new Date('2020-04-13T00:00:00.000Z'),
				new Date('2020-04-15T00:00:00.000Z'),
			];
			const minutestToSelect = 3*8*60;
			const dayLengthInMinutes = 8*60;
			const minDayFreeMinutes = 30;
			const actual: Date[] = selectDatesBySelector(
				[
					new Date('2020-02-12T00:00:00.000Z'),
					new Date('2020-02-13T00:00:00.000Z'),
					new Date('2020-02-15T00:00:00.000Z'),
					new Date('2020-02-16T00:00:00.000Z'),
					new Date('2020-04-12T00:00:00.000Z'),
					new Date('2020-04-13T00:00:00.000Z'),
					new Date('2020-04-14T00:00:00.000Z'),
					new Date('2020-04-15T00:00:00.000Z'),
				],
				new DatesRecordsMap([
					{
						type: 'office',
						from: new Date('2020-02-13T09:00:00.000Z'),
						to: new Date('2020-02-13T13:00:00.000Z'),
					},
					{
						type: 'office',
						from: new Date('2020-02-16T09:00:00.000Z'),
						to: new Date('2020-02-16T10:00:00.000Z'),
					},
					{
						type: 'office',
						from: new Date('2020-02-16T14:00:00.000Z'),
						to: new Date('2020-02-16T15:00:00.000Z'),
					},
				]),
				minutestToSelect,
				dayLengthInMinutes,
				minDayFreeMinutes,
				createMockSelector(),
			);

			expect(actual).toEqual(expected);
		});

		it('should select dates with given non-zero amount of free time (no records)', () => {
			const expected: Date[] = [
				new Date(2020, 1, 13),
				new Date(2020, 1, 16),
				new Date(2020, 3, 13),
			];
			const minutestToSelect = 3*8*60;
			const dayLengthInMinutes = 8*60;
			const minDayFreeMinutes = 30;
			const actual: Date[] = selectDatesBySelector(
				[
					new Date(2020, 1, 12),
					new Date(2020, 1, 13),
					new Date(2020, 1, 15),
					new Date(2020, 1, 16),
					new Date(2020, 3, 12),
					new Date(2020, 3, 13),
				],
				new DatesRecordsMap(),
				minutestToSelect,
				dayLengthInMinutes,
				minDayFreeMinutes,
				createMockSelector(),
			);

			expect(actual).toEqual(expected);
		});

		it('should not select any dates with given zero free time', () => {
			const expected: Date[] = [];
			const minutestToSelect = 0;
			const dayLengthInMinutes = 8*60;
			const minDayFreeMinutes = 30;
			const actual: Date[] = selectDatesBySelector(
				[
					new Date(2020, 1, 12),
					new Date(2020, 1, 13),
					new Date(2020, 1, 15),
					new Date(2020, 1, 16),
					new Date(2020, 3, 12),
					new Date(2020, 3, 13),
				],
				new DatesRecordsMap(),
				minutestToSelect,
				dayLengthInMinutes,
				minDayFreeMinutes,
				createMockSelector(),
			);

			expect(actual).toEqual(expected);
		});

		it('should not select any dates under free day time limit', () => {
			const expected: Date[] = [
				new Date('2020-04-15T00:00:00.000Z'),
				new Date('2020-04-14T00:00:00.000Z'),
				new Date('2020-04-12T00:00:00.000Z'),
			];
			const minutestToSelect = 3*8*60;
			const dayLengthInMinutes = 8*60;
			const minDayFreeMinutes = 30;
			const actual: Date[] = selectDatesBySelector(
				[
					new Date('2020-02-12T00:00:00.000Z'),
					new Date('2020-02-13T00:00:00.000Z'),
					new Date('2020-02-15T00:00:00.000Z'),
					new Date('2020-02-16T00:00:00.000Z'),
					new Date('2020-04-12T00:00:00.000Z'),
					new Date('2020-04-13T00:00:00.000Z'),
					new Date('2020-04-14T00:00:00.000Z'),
					new Date('2020-04-15T00:00:00.000Z'),
				],
				new DatesRecordsMap([
					{
						type: 'office',
						from: new Date('2020-02-13T06:00:00.000Z'),
						to: new Date('2020-02-13T14:00:00.000Z'),
					},
					{
						type: 'office',
						from: new Date('2020-02-16T09:00:00.000Z'),
						to: new Date('2020-02-16T12:00:00.000Z'),
					},
					{
						type: 'office',
						from: new Date('2020-02-16T12:25:00.000Z'),
						to: new Date('2020-02-16T17:00:00.000Z'),
					},
					{
						type: 'office',
						from: new Date('2020-04-13T06:00:00.000Z'),
						to: new Date('2020-04-13T18:00:00.000Z'),
					},
				]),
				minutestToSelect,
				dayLengthInMinutes,
				minDayFreeMinutes,
				createMockSelector(),
			);

			expect(actual).toEqual(expected);
		});
	});

	describe('isWorkingDay', () => {

		it('should return false for empty working days array', () => {
			const workingDays: number[] = [];

			expect(isWorkingDay(workingDays, new Date(2020, 0, 24))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(1987, 5, 17))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2025, 12, 2))).toEqual(false);
		});

		it('should return true for Monday dates and false for everything else', () => {
			const workingDays: number[] = [1];

			expect(isWorkingDay(workingDays, new Date(2020, 1, 3))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 16))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 24))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 4))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 17))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 25))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 5))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 18))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 26))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 6))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 19))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 27))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 7))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 20))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 28))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 8))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 21))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 29))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 9))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 22))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 30))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 10))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 23))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 31))).toEqual(true);
		});

		it('should return true for Tuesday to Friday dates and false for weekend days', () => {
			const workingDays: number[] = [ 2, 3, 4, 5 ];

			expect(isWorkingDay(workingDays, new Date(2020, 1, 3))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 16))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 24))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 4))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 17))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 25))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 5))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 18))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 26))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 6))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 19))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 27))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 7))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 20))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 28))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 8))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 21))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 29))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 9))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 22))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 30))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 10))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 23))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 31))).toEqual(false);
		});

		it('should return false for array including values only outside of range <0,6>', () => {
			const workingDays: number[] = [ -23 -1, 7, 18 ];

			expect(isWorkingDay(workingDays, new Date(2020, 1, 3))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 16))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 24))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 4))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 17))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 25))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 5))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 18))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 26))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 6))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 19))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 27))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 7))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 20))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 28))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 8))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 21))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 29))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 9))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 22))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 30))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 10))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 23))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 31))).toEqual(false);
		});

		it('should return true for Wednesday and Friday dates ignoring array values outside of range <0,6>', () => {
			const workingDays: number[] = [ -23 -1, 3, 5, 7, 18 ];

			expect(isWorkingDay(workingDays, new Date(2020, 1, 3))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 16))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 24))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 4))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 17))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 25))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 5))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 18))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 26))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 6))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 19))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 27))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 7))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 20))).toEqual(true);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 28))).toEqual(true);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 8))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 21))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 29))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 9))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 22))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 30))).toEqual(false);

			expect(isWorkingDay(workingDays, new Date(2020, 1, 10))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2020, 10, 23))).toEqual(false);
			expect(isWorkingDay(workingDays, new Date(2024, 5, 31))).toEqual(false);
		});

	});

});
