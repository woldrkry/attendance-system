import initializeDataDemo from 'domain-model/dist/initialization/initDataDemo';
import { RecordAttributes } from 'domain-model/dist/model/Record/RecordAttributes';
import getRecordsByProjectIdAndEmployeeId from '../../../src/Records/getRecordsByProjectIdAndEmployeeId';
import { connect, close, dropAll } from '../../database';

describe('getRecordsByProjectIdAndEmployeeId', () => {

	beforeAll(async () => {
		await connect();
	});

	afterAll(async () => {
		await close();
	});

	beforeEach(async () => {
		await initializeDataDemo();
	});

	afterEach(async () => {
		await dropAll();
	});

	it('should get records in given period ordered ascending by from', async () => {
		const expected = [
			{
				projectId: 1,
				employeeId: 1,
				type: 'office',
				from: new Date('2020-01-10T09:10:00.000Z'),
				to: new Date('2020-01-10T11:10:00.000Z'),
			},
			{
				projectId: 1,
				employeeId: 1,
				type: 'office',
				from: new Date('2020-01-11T09:10:00.000Z'),
				to: new Date('2020-01-11T11:10:00.000Z'),
			},
		];

		const projectId = 1;
		const employeeId = 1;
		const from = new Date('2020-01-01T00:00:00.000Z');
		const to = new Date('2020-01-31T00:00:00.000Z');
		const actualCandidate: RecordAttributes[] = await getRecordsByProjectIdAndEmployeeId(
			projectId,
			employeeId,
			from,
			to,
		);
		const actual = actualCandidate.map((candidate) => ({
			projectId: candidate.projectId,
			employeeId: candidate.employeeId,
			type: candidate.type,
			from: candidate.from,
			to: candidate.to,
		}));
	
		expect(actual).toEqual(expected);
	});

	it('should not get any records', async () => {
		const expected: any[] = [];

		const projectId = 1;
		const employeeId = 1;
		const from = new Date('2020-10-01T00:00:00.000Z');
		const to = new Date('2020-10-31T00:00:00.000Z');
		const actualCandidate: RecordAttributes[] = await getRecordsByProjectIdAndEmployeeId(
			projectId,
			employeeId,
			from,
			to,
		);
		const actual = actualCandidate.map((candidate) => ({
			projectId: candidate.projectId,
			employeeId: candidate.employeeId,
			type: candidate.type,
			from: candidate.from,
			to: candidate.to,
		}));
	
		expect(actual).toEqual(expected);
	});
});
