import initializeDataDemo from 'domain-model/dist/initialization/initDataDemo';
import findContractByProjectAssignmentId from '../../../src/Contracts/findContractByProjectAssignmentId';
import { connect, close, dropAll } from '../../database';

describe('findContractByProjectAssignmentId', () => {

	beforeAll(async () => {
		await connect();
	});

	afterAll(async () => {
		await close();
	});

	beforeEach(async () => {
		await initializeDataDemo();
	});

	afterEach(async () => {
		await dropAll();
	});

	it('should get contract by given assignment id', async () => {
		const expected = {
			extent: 0.5,
			from: new Date("2020-1-1"),
			to: new Date("2023-1-1"),
			type: 'Administrative',
		};

		const actualCandidate = await findContractByProjectAssignmentId(1);
		const actual = {
			extent: actualCandidate?.extent,
			from: actualCandidate?.from,
			to: actualCandidate?.to,
			type: actualCandidate?.type,
		};
		
		expect(actual).toEqual(expected);
	});

	it('should not get any contract by non existing id', async () => {
		expect(await findContractByProjectAssignmentId(100)).toEqual(null);
	});
});
